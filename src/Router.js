export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		const tab = element.querySelectorAll('a');
		for (let i = 0; i < tab.length; i++) {
			tab[i].addEventListener('click', event => {
				event.preventDefault();
				console.log(
					"tab[i].getAttribute('href')" + tab[i].getAttribute('href')
				);
				Router.navigate(tab[i].getAttribute('href'));
			});
		}
	}
}
