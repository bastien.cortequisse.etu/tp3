import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";

console.log(
	document.querySelector('footer > div > a ~ a').getAttribute('href')
);

document
	.querySelector('header > nav > ul > li > a')
	.setAttribute('class', 'active');

document.querySelector('section').setAttribute('style', '');

document
	.querySelector('section > article > button')
	.addEventListener('click', event => {
		event.preventDefault();
		document.querySelector('section').setAttribute('style', 'display:none');
	});

const pizzaList = new PizzaList([]);
const aboutPage = new Component('section', null, 'Ce site est génial');
const pizzaForm = new Component(
	'section',
	null,
	'Ici vous pourrez ajouter une pizza'
);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

Router.menuElement = document.querySelector('.mainMenu');
